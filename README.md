# quic-rpc库的高级封装

## 介绍
quic-rpc-wrap通过属性宏的方式简化quic-rpc的实现，消除了所有不必要的模板代码。
编写一次服务端的实现，自动生成客户端方法的存根，与本地函数调用几乎没有差异。

## 特点
1. 支持一对一请求和响应；
2. 支持客户端流模式；
3. 支持服务端流模式；
4. 支持客户端和服务端的双向流模式；
5. 支持模块化，可用于生产环境；
6. 高性能，支持quic协议(http3)、http2协议和内存、命名管道等传输，其中quic协议比http2性能提升了30%；
7. 人体工程学的设计，代码整洁，使用简单。


## 运行示例

### 简单的内存传输示例
```shell
cargo run --package quic-rpc-example --bin flume --features server,client,flume
```

### HTTP2协议传输示例
```shell
cargo run --package quic-rpc-example --bin hyper --features server,client,hyper
```

### QUIC协议传输示例
```shell
cargo run --package quic-rpc-example --bin quinn --features server,client,quinn
```

### 基于P2P（点对点传输）的Iroh示例

IROH是一个库，为您提供了按公共密钥拨号的API。您说“连接到该手机”，IROH将为您找到并维护最快的连接，无论它在哪里。
```shell
cargo run --package quic-rpc-example --bin iroh --features server,client,iroh
```

### 命名管道示例（可用于进程间通信）
```shell
cargo run --package quic-rpc-example --bin pipe --features server,client,pipe
```


如果启用`server`特性，则会生成服务端模板代码；
如果启用`client`特性，则会生成客户端模板代码。



## 运行测试

```shell
cargo test
```


## 关于证书

如果您的应用使用了QUIC协议传输，在调试过程中可能会比较困难，我们增加了一个可以生成自签名证书的cargo扩展，以便于您在开发环境中使用。
如果想要生成证书文件，首先需要安装cargo扩展程序：
```shell
cargo install quic-rpc-wrap
```
然后可以使用下面的命令来生成自签名证书文件：
```shell
cargo server-cert gen -n localhost -n your.domain -c /path/to/your_cert.crt -k /path/to/your/private_key.key
```
如果想要获取命令的帮助，可以运行：
```shell
cargo server-cert gen -h
```