use clap::{Parser, Subcommand};

#[derive(Clone, Debug, Subcommand)]
#[command(author, version)]
enum Command {
    /// Generate server certificate file.
    Gen {
        /// Subject alt names(Can pass multiple)
        #[arg(short = 'n', long, default_value = "localhost")]
        subject_alt_names: Vec<String>,
        /// The file path of certificate private key
        #[arg(short = 'k', long, default_value = "server.key")]
        key_path: String,
        /// Certificate file path
        #[arg(short = 'c', long, default_value = "server.crt")]
        cert_path: String,
    },
}

#[derive(Parser, Debug)]
#[command(bin_name = "cargo")]
enum Cargo {
    #[command(subcommand)]
    ServerCert(Command),
}

fn main() -> quic_rpc_utils::Result<()> {
    let Cargo::ServerCert(Command::Gen {
        subject_alt_names,
        key_path,
        cert_path,
    }) = Parser::parse();
    let (cert, key) = quic_rpc_utils::gen_server_cert(&subject_alt_names.iter().map(|i| i.as_str()).collect::<Vec<_>>())?;
    quic_rpc_utils::save_cert_file(&cert,&key,cert_path,key_path)?;
    Ok(())
}
