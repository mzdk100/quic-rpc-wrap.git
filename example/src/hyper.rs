//! http协议传输

mod common;

use common::{call_service::client_demo, declare_service::app::*};
use quic_rpc_utils::{HyperConnector, HyperListener, Result};
use std::net::SocketAddr;

#[tokio::main]
async fn main() -> Result<()> {
    let addr: SocketAddr = "127.0.0.1:9988".parse()?;
    let _server_conn = HyperListener::serve(&addr)?;
    let _client_conn = HyperConnector::new("http://127.0.0.1:9988".parse()?);

    #[cfg(all(feature = "server", not(feature = "client")))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::<AppService, _>::new(_server_conn);
        run_server(server).await
    }
    #[cfg(all(feature = "client", not(feature = "server")))]
    {
        use quic_rpc_utils::RpcClient;
        let client = RpcClient::<AppService, _>::new(_client_conn);
        client_demo(client).await?;
    }
    #[cfg(all(feature = "client", feature = "server"))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::new(_server_conn.clone());
        tokio::spawn(run_server::<AppService, _>(server));

        use quic_rpc_utils::RpcClient;
        let client = RpcClient::new(_client_conn);
        client_demo(client).await?;
    }
    Ok(())
}
