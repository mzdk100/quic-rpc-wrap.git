//! 进程间RPC（使用命名管道实现）

mod common;

use common::{call_service::client_demo, declare_service::app::*};
use quic_rpc_utils::Result;

#[tokio::main]
async fn main() -> Result<()> {
    const PIPE_NAME: &str = r"\\.\PIPE\RPC_PIPE_TEST";
    let _server_conn = quic_rpc_utils::PipeListener::serve(PIPE_NAME)?;
    let _client_conn = quic_rpc_utils::PipeConnector::new(PIPE_NAME)?;
    #[cfg(all(feature = "server", not(feature = "client")))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::<AppService, _>::new(_server_conn);
        run_server(server).await
    }
    #[cfg(all(feature = "client", not(feature = "server")))]
    {
        use quic_rpc_utils::RpcClient;
        let client = RpcClient::<AppService, _>::new(_client_conn);
        client_demo(client).await?;
    }
    #[cfg(all(feature = "client", feature = "server"))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::new(_server_conn.clone());
        tokio::spawn(run_server::<AppService, _>(server));

        use quic_rpc_utils::RpcClient;
        let client = RpcClient::new(_client_conn);
        client_demo(client).await?;
    }

    Ok(())
}
