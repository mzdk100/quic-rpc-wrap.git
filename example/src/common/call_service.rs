use crate::common::declare_service::app::*;
use quic_rpc_utils::{Connector, Result, RpcClient};

#[cfg(feature = "client")]
pub(crate) async fn client_demo<C: Connector<AppService>>(
    client: RpcClient<AppService, C>,
) -> Result<()> {
    let client = AppServiceClient::new(&client);

    let mut s = 0i128;
    let cost = std::time::Instant::now();
    for i in 0..100000 {
        s += client.add(2, i).await?;
        if i % 20000 == 0 {
            println!("{i},{s}");
        }
    }
    println!("res:{:?},cost:{:?}", s, cost.elapsed());

    let res = client
        .sum(20)
        .await?
        .put(1)
        .await
        .put(2)
        .await
        .result()
        .await?;
    println!("Sum: {:?}", res);

    let mut countdown = client.countdown(10, "Start countdown.".to_string()).await?;
    while let Some(item) = countdown.next().await {
        println!("{:?}", item)
    }
    let _ = dbg!(client.child.hello("abc".to_string()).await);
    let (mut update, mut res) = client.child.echo("test".to_string(), 65u8).await?;
    update.put(0).await;
    update.put(1).await;
    dbg!(res.next().await);
    dbg!(res.next().await);

    Ok(())
}
