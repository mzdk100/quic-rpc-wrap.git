//! http协议传输

mod common;

use common::{call_service::client_demo, declare_service::app::*};

use quic_rpc_utils::{IrohConnector, IrohEndpoint, IrohListener, Result, SecretKey};

#[tokio::main]
async fn main() -> Result<()> {
    let alpn = b"quic-rpc-wrap/iroh/test".to_vec();

    let server = IrohEndpoint::builder()
        .secret_key(SecretKey::generate(rand::thread_rng()))
        .alpns(vec![alpn.clone()])
        .bind()
        .await?;
    let client = IrohEndpoint::builder()
        .secret_key(SecretKey::generate(rand::thread_rng()))
        .alpns(vec![alpn.clone()])
        .bind()
        .await?;
    let node_addr = server.node_addr().await?;

    let _server_conn = IrohListener::new(server)?;
    let _client_conn = IrohConnector::new(client, node_addr, alpn);

    #[cfg(all(feature = "server", not(feature = "client")))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::<AppService, _>::new(_server_conn);
        run_server(server).await
    }
    #[cfg(all(feature = "client", not(feature = "server")))]
    {
        use quic_rpc_utils::RpcClient;
        let client = RpcClient::<AppService, _>::new(_client_conn);
        client_demo(client).await?;
    }
    #[cfg(all(feature = "client", feature = "server"))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::new(_server_conn.clone());
        tokio::spawn(run_server::<AppService, _>(server));

        use quic_rpc_utils::RpcClient;
        let client = RpcClient::new(_client_conn);
        client_demo(client).await?;
    }

    Ok(())
}
