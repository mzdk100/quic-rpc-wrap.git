//! 内存RPC（单个进程内）

mod common;

use common::{call_service::client_demo, declare_service::app::*};
use quic_rpc_utils::Result;

#[tokio::main]
async fn main() -> Result<()> {
    let (_server_conn, _client_conn) = quic_rpc_utils::flume_channel(1);
    #[cfg(all(feature = "server", not(feature = "client")))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::<AppService, _>::new(_server_conn);
        run_server(server).await
    }
    #[cfg(all(feature = "client", not(feature = "server")))]
    {
        use quic_rpc_utils::RpcClient;
        let client = RpcClient::<AppService, _>::new(_client_conn);
        client_demo(client).await?;
    }
    #[cfg(all(feature = "client", feature = "server"))]
    {
        use quic_rpc_utils::{RpcServer, run_server};
        let server = RpcServer::new(_server_conn.clone());
        tokio::spawn(run_server::<AppService, _>(server));

        use quic_rpc_utils::RpcClient;
        let client = RpcClient::new(_client_conn);
        client_demo(client).await?;
    }

    Ok(())
}
